<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\UserNotDefinedException;

use Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('register', 'login');
    }

    // /**
    //  * @param \Illuminate\Http\Request $request
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function register(Request $request): JsonResponse
    // {
    //     $payload = $request->validate([
    //         'name' => 'required|min:2',
    //         'email' => 'required|email|unique:users',
    //         'password' => 'required|min:8',
    //     ]);

    //     $payload['password'] = Hash::make($payload['password']);
    //     $user = User::create($payload);

    //     $token = auth()->login($user);

    //     return $this->respondWithToken($token);
    // }

    // /**
    //  * @param \Illuminate\Http\Request $request
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function login(Request $request): JsonResponse
    // {
    //     $payload = $request->validate([
    //         'email' => 'required|email',
    //         'password' => 'required|min:8',
    //     ]);

    //     if (! $token = auth()->attempt($payload)) {
    //         return response()->json(['message' => 'Invalid username or password.'], 401);
    //     }

    //     return $this->respondWithToken($token);
    // }

    // /**
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function user(): JsonResponse
    // {
    //     return response()->json(auth()->user());
    // }

    // /**
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function refresh(): JsonResponse
    // {
    //     return $this->respondWithToken(auth()->refresh());
    // }

    // /**
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function logout(): JsonResponse
    // {
    //     auth()->logout();

    //     return response()->json(['message' => 'Successfully logged out.']);
    // }

    // /**
    //  * @param string $token
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // protected function respondWithToken(string $token): JsonResponse
    // {
    //     return response()->json([
    //         'access_token' => $token,
    //         'valid_until' => auth()->factory()->getTTL() * 60,
    //     ]);
    // }

     /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Wrong Username'], 401);
        }

        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:1',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        return response()->json([
            'token' => auth()->attempt($validator->validated()),
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    // protected function verifyToken(Request $request){
    //     $header = $request->header('Authorization');
    //     if(!$header == ""){
    //         return response()->json(["staus"=>"fail", "msg"=>"verification failed"], 400);
    //     }
    //     $token = explode(" ",$header);
    //     if($token[1] == ""){
    //         return response()->json(["staus"=>"fail", "msg"=>"verification failed"], 401);
    //     }

    // }

}
