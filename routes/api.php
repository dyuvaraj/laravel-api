<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('refresh ', 'AuthController@refresh');
    Route::post('logout', 'AuthController@logout');
    Route::get('user-profile', 'AuthController@userProfile');
    Route::post('product/add', 'ProductController@store');
    Route::get('product/showall', 'ProductController@showAll');
    Route::post('product/imageupload', 'ProductController@storeImage');
    Route::post('contact/save', 'ContactController@store');
});